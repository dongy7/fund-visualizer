# Fund Visualizer

## About

This is a single-page application that allows users to visually track their progress
on a map. A live version of the app can be accessed [here](https://fund-visualizer.herokuapp.com/).
The app was built using [React](https://facebook.github.io/react/) and [Redux](https://github.com/reactjs/redux).


## Building locally

```
git clone https://github.com/dongy7/fund-visualizer.git
cd fund-visulizer/
npm install
npm start
```
Go to http://localhost:3000/
